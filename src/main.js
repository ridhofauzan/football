import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import team from "./modules/team";

import { registerModules } from "./register-modules";

registerModules({
  team: team,
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
