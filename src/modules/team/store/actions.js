import axios from 'axios';

const getAreas = ({ commit }) => {
    axios.get('http://api.football-data.org/v2/areas', {
         headers: {
             'X-Auth-Token': 'e70a3e4576994f3ab02dceb86c91129a'
         }
     }).then(response => {
         commit('SET_AREAS', response.data.areas)
    });
}

const getTeams = ({ commit }, id) => {
    axios.get(`http://api.football-data.org/v2/teams`, {
         headers: {
             'X-Auth-Token': 'e70a3e4576994f3ab02dceb86c91129a'
         }
     }).then(response => {
        commit('SET_TEAMS', response.data.teams.filter(teams => teams.area.id == id))
    });
}

const getSelectedTeam = ({ commit }, id) => {
     axios.get(`http://api.football-data.org/v2/teams/${id}`, {
         headers: {
             'X-Auth-Token': 'e70a3e4576994f3ab02dceb86c91129a'
         }
     }).then(response => {
        commit('SET_SELECTED_TEAM', response)
    });
}

export default {
    getAreas,
    getTeams,
    getSelectedTeam
}