// import * as types from "./mutation-types";

export default {
//   [types.ADD_AREA](state, area) {
//     state.areas.push(area);
//   },

//   [types.REMOVE_AREA](state, id) {
//     state.areas = state.areas.filter(area => area.id !== id);
//   }
    SET_AREAS(state, payload) {
        state.areas = payload
    },
    SET_TEAMS(state, payload) {
        state.teams = payload
    },
    SET_TEAMS_BY_AREA(state, id) {
        state.teams = state.teams.filter(team => team.area.id == id);
    },
    SET_SELECTED_TEAM(state, payload) {
        state.selectedTeam = payload
    }
};