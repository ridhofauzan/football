export default {
    allAreas: (state) => state.areas,
    allTeams: (state) => state.teams,
    selectedTeam: (state) => state.selectedTeam
}