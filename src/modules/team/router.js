const Module = () => import("./Module.vue");
const Home = () => import("./views/Home.vue");
const TeamDetails = () => import("./views/TeamDetails.vue");

const moduleRoute = {
  path: "/team",
  component: Module,
  children: [
    {
      path: "/",
      component: Home
    },
    {
      path: ":id",
      component: TeamDetails
    },
  ]
};

export default router => {
  router.addRoutes([moduleRoute]);
};